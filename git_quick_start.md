# Git Quick Start
1) Регистрируетесь на github.com  
2) Заходите на https://github.com/erlong15/otus-linux  
3) Нажимате `Fork` (кнопка сверху справа) - в вашем репозитории появится копия проекта
4) На рабочей машине делаете `git clone <ссылка на ваш репозиторий>` - кнопка "clone or download"  
5) Вносите правки, работаете над проектом, делаете 
```
git add .
git commit -m <comment> -a
```  
6)  По окончании работы делаете `git push`   
7)  В "Чате с преподавателем" отсылаете ссылку на ваш репозиторий  

Полезные ссылки по git:  
https://git-scm.com/book/ru/v1/%D0%92%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D0%B8%D0%B5  
https://githowto.com/ru  
https://learngitbranching.js.org/  